/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.activiti.demo.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.exception.ErrorCode;
import io.renren.common.page.PageData;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.modules.activiti.demo.dto.LeaveDTO;
import io.renren.modules.activiti.demo.service.LeaveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 */
@RestController
@RequestMapping("demo/leave")
@Api(tags="请假")
public class LeaveController {
    @Autowired
    private LeaveService leaveService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    @RequiresPermissions("activiti:leave:all")
    public Result<PageData<LeaveDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params){
        PageData<LeaveDTO> page = leaveService.page(params);

        return new Result<PageData<LeaveDTO>>().ok(page);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("activiti:leave:all")
    public Result save(@RequestBody LeaveDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);
        if(StringUtils.isEmpty(dto.getProcessDefinationKey())){
            return new Result().error(ErrorCode.PARAMS_GET_ERROR);
        }
        leaveService.saveLeave(dto);

        return new Result();
    }


    @GetMapping("{id}")
    @ApiOperation("信息")
    public Result<LeaveDTO> get(@PathVariable("id") Long id){
        LeaveDTO data = leaveService.get(id);
        return new Result<LeaveDTO>().ok(data);
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        leaveService.deleteBatchIds(Arrays.asList(ids));

        return new Result();
    }

}