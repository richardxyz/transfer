/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.activiti.demo.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.activiti.demo.entity.LeaveEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface LeaveDao extends BaseDao<LeaveEntity> {
	
}