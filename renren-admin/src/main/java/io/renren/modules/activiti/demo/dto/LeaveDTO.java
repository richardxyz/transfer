/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.activiti.demo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 */
@Data
@ApiModel(value = "")
public class LeaveDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "id")
	private Long id;

	@ApiModelProperty(value = "创建者")
	private Long creator;

	@ApiModelProperty(value = "创建时间")
	private Date createDate;

	@ApiModelProperty(value = "流程实例ID")
	private String instanceId;

	@ApiModelProperty(value = "请假天数")
	private Integer leaveDays;

	@ApiModelProperty(value = "请假原因")
	private String leaveReason;

	@ApiModelProperty(value = "开始日期")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date beginDate;

	@ApiModelProperty(value = "结束日期")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endDate;

	@ApiModelProperty(value = "流程定义Key")
	private String processDefinationKey;


}