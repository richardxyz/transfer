/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.activiti.demo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("tb_leave")
public class LeaveEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 流程实例ID
     */
	private String instanceId;
    /**
     * 请假天数
     */
	private Integer leaveDays;
    /**
     * 请假原因
     */
	private String leaveReason;
    /**
     * 开始日期
     */
	private Date beginDate;
    /**
     * 结束日期
     */
	private Date endDate;
}