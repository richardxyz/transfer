/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.activiti.demo.service;

import io.renren.common.service.CrudService;
import io.renren.modules.activiti.demo.dto.LeaveDTO;
import io.renren.modules.activiti.demo.entity.LeaveEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 */
public interface LeaveService extends CrudService<LeaveEntity, LeaveDTO> {

    void saveLeave(LeaveDTO dto);
}