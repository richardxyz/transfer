/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.activiti.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.activiti.demo.dao.LeaveDao;
import io.renren.modules.activiti.demo.dto.LeaveDTO;
import io.renren.modules.activiti.demo.entity.LeaveEntity;
import io.renren.modules.activiti.demo.service.LeaveService;
import io.renren.modules.activiti.dto.ProcessInstanceDTO;
import io.renren.modules.activiti.service.ActRunningService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 */
@Service
public class LeaveServiceImpl extends CrudServiceImpl<LeaveDao, LeaveEntity, LeaveDTO> implements LeaveService {

    @Autowired
    private ActRunningService actRunningService;

    @Override
    public QueryWrapper<LeaveEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<LeaveEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveLeave(LeaveDTO dto) {
        this.save(dto);
        String businessKey = dto.getId().toString();
        Map<String, Object> map = new HashMap<>();
        map.put("days", dto.getLeaveDays());
        ProcessInstanceDTO processInstanceDTO = actRunningService.startOfBusinessKey(dto.getProcessDefinationKey(),businessKey, map);
        dto.setInstanceId(processInstanceDTO.getProcessInstanceId());
        this.update(dto);
    }
}