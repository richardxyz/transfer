/**
 * Copyright (c) 2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.activiti.dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Jone
 */
@Data
@Api(tags="活动节点信息")
public class ActivityDTO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "活动ID")
    private String activityId;

    @ApiModelProperty(value = "活动名称")
    private String activtyName;
}
