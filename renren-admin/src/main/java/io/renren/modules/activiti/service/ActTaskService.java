/**
 * Copyright (c) 2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.activiti.service;

import io.renren.common.constant.Constant;
import io.renren.common.exception.ErrorCode;
import io.renren.common.exception.RenException;
import io.renren.common.page.PageData;
import io.renren.common.service.impl.BaseServiceImpl;
import io.renren.common.utils.MessageUtils;
import io.renren.modules.activiti.dto.ActivityDTO;
import io.renren.modules.activiti.dto.TaskDTO;
import io.renren.modules.security.user.SecurityUser;
import io.renren.modules.sys.service.SysRoleUserService;
import org.activiti.engine.*;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.bpmn.behavior.UserTaskActivityBehavior;
import org.activiti.engine.impl.javax.el.ExpressionFactory;
import org.activiti.engine.impl.javax.el.ValueExpression;
import org.activiti.engine.impl.juel.ExpressionFactoryImpl;
import org.activiti.engine.impl.juel.SimpleContext;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.PvmActivity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.pvm.process.TransitionImpl;
import org.activiti.engine.impl.task.TaskDefinition;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskInfo;
import org.activiti.engine.task.TaskQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 任务管理
 *
 * @author Jone
 */
@Service
public class ActTaskService extends BaseServiceImpl {
    @Autowired
    protected TaskService taskService;
    @Autowired
    protected HistoryService historyService;
    @Autowired
    protected RepositoryService repositoryService;
    @Autowired
    protected RuntimeService runtimeService;
    @Autowired
    private SysRoleUserService sysRoleUserService;

    /**
     * 根据参数获取当前运行的任务信息
     *
     * @param params
     * @return
     */
    public PageData<TaskDTO> page(Map<String, Object> params) {
        String userId = (String) params.get("userId");
        Integer curPage = 1;
        Integer limit = 10;
        if (params.get(Constant.PAGE) != null) {
            curPage = Integer.parseInt((String) params.get(Constant.PAGE));
        }
        if (params.get(Constant.LIMIT) != null) {
            limit = Integer.parseInt((String) params.get(Constant.LIMIT));
        }
        TaskQuery taskQuery = taskService.createTaskQuery();
        if (StringUtils.isNotEmpty(userId)) {
            taskQuery.taskAssignee(userId);
        }
        if (StringUtils.isNotEmpty((String)params.get("taskName"))){
            taskQuery.taskNameLike("%"+(String)params.get("taskName")+"%");
        }
        if(StringUtils.isNotEmpty((String)params.get("isRoleGroup"))&&"1".equals(params.get("isRoleGroup"))){
            List<Long> listRoles = sysRoleUserService.getRoleIdList(SecurityUser.getUserId());
            List<String> listStr = new ArrayList<>();
            for(Long role : listRoles){
                listStr.add(role.toString());
            }
            listStr.add(SecurityUser.getUserId().toString());
            if(!listStr.isEmpty()){
                taskQuery.taskCandidateGroupIn(listStr);
            } else {
                return new PageData<>(new ArrayList<>(), 0);
            }
        }
        taskQuery.orderByTaskCreateTime().desc();
        List<Task> list = taskQuery.listPage((curPage - 1) * limit, limit);
        List<TaskDTO> listDto = new ArrayList<>();
        for (Task task : list) {
            TaskDTO dto = new TaskDTO();
            this.convertTaskInfo(task, dto);
            listDto.add(dto);
        }
        return new PageData<>(listDto, (int) taskQuery.count());
    }

    /**
     * 获取任务详情信息
     *
     * @param id
     * @return
     */
    public TaskDTO taskDetail(String id) {
        Task task = taskService.createTaskQuery().taskId(id).singleResult();
        TaskDTO dto = new TaskDTO();
        this.convertTaskInfo(task, dto);
        return dto;
    }

    /**
     * 签收任务
     *
     * @param taskId
     */
    public void claimTask(String taskId) {
        String userId = SecurityUser.getUserId().toString();
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        if(StringUtils.isNotEmpty(task.getAssignee())){
            throw new RenException(ErrorCode.TASK_CLIME_FAIL);
        }
        taskService.claim(taskId, userId);
    }

    /**
     * 释放任务
     *
     * @param taskId
     */
    public void unclaimTask(String taskId) {
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        String procInstId = task.getProcessInstanceId();
        String processDefinitionId = historyService.createHistoricProcessInstanceQuery().processInstanceId(procInstId)
                .singleResult().getProcessDefinitionId();
        ProcessDefinitionEntity def = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService).getDeployedProcessDefinition(processDefinitionId);
        ExecutionEntity execution = (ExecutionEntity) runtimeService.createProcessInstanceQuery().processInstanceId(procInstId).singleResult();
        String activitiId = execution.getActivityId();
        List<ActivityImpl> activitiList = def.getActivities();
        List<String> lstGroupId = new ArrayList<>();
        for (ActivityImpl activityImpl : activitiList) {
            if (activitiId.equals(activityImpl.getId())) {
                TaskDefinition taskDef = ((UserTaskActivityBehavior) activityImpl.getActivityBehavior()).getTaskDefinition();
                Set<Expression> groupIds = taskDef.getCandidateGroupIdExpressions();
                for (Expression exp : groupIds) {
                    lstGroupId.add(exp.getExpressionText());
                }
            }
        }
        if(lstGroupId.isEmpty()){
            throw new RenException(ErrorCode.UNCLAIM_ERROR_MESSAGE);
        }
        taskService.unclaim(taskId);
    }

    /**
     * 处理任务
     *
     * @param taskId
     * @param comment
     */
    public void completeTask(String taskId, String comment) {
        String userId = SecurityUser.getUserId().toString();
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        if(StringUtils.isNotEmpty(task.getAssignee())){
            taskService.setAssignee(taskId, userId);
        }
        if(StringUtils.isNotEmpty(comment)){
            taskService.addComment(taskId, task.getProcessInstanceId(), comment);
        }
        taskService.complete(taskId);
    }

    /**
     * 任务委托
     *
     * @param taskId
     * @param depositorId
     * @param assignee
     */
    public void changeTaskAssignee(String taskId, String depositorId, String assignee) {
        taskService.setOwner(taskId, depositorId);
        taskService.setAssignee(taskId, assignee);
    }

    /**
     * 获取下一环节的用户任务信息
     *
     * @param currentTaskId
     * @return
     */
    public List<TaskDTO> getNextTask(String currentTaskId) {
        TaskDefinition taskDef = null;
        Task task = taskService.createTaskQuery().taskId(currentTaskId).singleResult();
        String procInstId = task.getProcessInstanceId();
        String processDefinitionId = historyService.createHistoricProcessInstanceQuery().processInstanceId(procInstId)
                .singleResult().getProcessDefinitionId();
        ProcessDefinitionEntity def = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService).getDeployedProcessDefinition(processDefinitionId);
        ExecutionEntity execution = (ExecutionEntity) runtimeService.createProcessInstanceQuery().processInstanceId(procInstId).singleResult();
        String activitiId = execution.getActivityId();
        List<ActivityImpl> activitiList = def.getActivities();
        List<TaskDTO> list = new ArrayList<>();
        for (ActivityImpl activityImpl : activitiList) {
            if (activitiId.equals(activityImpl.getId())) {
                taskDef = nextTaskDefinition(activityImpl, activityImpl.getId(), null, procInstId);
                if (null != taskDef) {
                    TaskDTO taskDTO = new TaskDTO();
                    taskDTO.setTaskName(taskDef.getNameExpression().getExpressionText());
                    Set<Expression> groupIds = taskDef.getCandidateGroupIdExpressions();
                    List<String> lstGroupId = new ArrayList<String>();
                    for (Expression exp : groupIds) {
                        lstGroupId.add(exp.getExpressionText());
                    }
                    taskDTO.setLstGroupId(lstGroupId);
                    Set<Expression> userIds = taskDef.getCandidateUserIdExpressions();
                    List<String> lstUserIds = new ArrayList<String>();
                    for (Expression exp : userIds) {
                        lstUserIds.add(exp.getExpressionText());
                    }
                    taskDTO.setLstUserIds(lstUserIds);
                    taskDTO.setActivityId(activitiId);
                    list.add(taskDTO);
                }
            }
        }
        return list;
    }



    /**
     * 查询流程启动时设置排他网关判断条件信息
     *
     * @param gatewayId         排他网关Id信息, 流程启动时设置网关路线判断条件key为网关Id信息
     * @param processInstanceId 流程实例Id信息
     * @return
     */
    private String getGatewayCondition(String gatewayId, String processInstanceId) {
        Execution execution = runtimeService.createExecutionQuery().processInstanceId(processInstanceId).singleResult();
        Object object = runtimeService.getVariable(execution.getId(), gatewayId);
        return object == null ? "" : object.toString();
    }

    /**
     * 根据key和value判断el表达式是否通过信息
     *
     * @param key   el表达式key信息
     * @param el    el表达式信息
     * @param value el表达式传入值信息
     * @return
     */
    public boolean isCondition(String key, String el, String value) {
        ExpressionFactory factory = new ExpressionFactoryImpl();
        SimpleContext context = new SimpleContext();
        context.setVariable(key, factory.createValueExpression(value, String.class));
        ValueExpression e = factory.createValueExpression(context, el, boolean.class);
        return (Boolean) e.getValue(context);
    }

    /**
     * 获取流程参数
     *
     * @param taskId
     * @param variableName
     * @return
     */
    public Map<String, Object> getTaskVariables(String taskId, String variableName) {
        Map<String, Object> map = null;
        if (StringUtils.isNotBlank(variableName)) {
            Object value = taskService.getVariable(taskId, variableName);
            if(null != value){
                map = new HashMap<>();
                map.put(variableName, value);
            }
        } else {
            map = taskService.getVariables(taskId);
        }

        return map;

    }

    /**
     * 更新任务变量
     * @param taskDTO
     */
    public void updateTaskVariable(TaskDTO taskDTO) {
        taskService.setVariables(taskDTO.getTaskId(), taskDTO.getParams());
    }

    public void setTaskVariable(String taskId, String key, Object value){
        TaskInfo taskInfo =  taskService.createTaskQuery().taskId(taskId).singleResult();
        runtimeService.setVariable(taskInfo.getExecutionId(), key, value);
    }

    /**
     * 根据当前任务ID，查询可以回退的用户任务节点信息
     * @param taskId
     * @return
     * @throws Exception
     */
    public List<ActivityDTO> queryBackUserTasks(String taskId) throws Exception {
        List<ActivityImpl> rtnList = null;

        if (false) {
            //会签任务暂未实现
            //rtnList = new ArrayList<ActivityImpl>();
        } else {
            rtnList = iteratorBackActivity(taskId, findActivitiImpl(taskId, null), new ArrayList<ActivityImpl>(), new ArrayList<ActivityImpl>());
        }
        List<ActivityDTO> listAct = new ArrayList<>();
        if(!rtnList.isEmpty()){
            for(ActivityImpl activity: rtnList){
                ActivityDTO dto = new ActivityDTO();
                dto.setActivityId(activity.getId());
                dto.setActivtyName((String)activity.getProperty("name"));
                listAct.add(dto);
            }
        }
        return listAct;
    }

    /**
     * 迭代循环流程树结构，查询当前节点可驳回的任务节点
     *
     * @param taskId       当前任务ID
     * @param currActivity 当前活动节点
     * @param rtnList      存储回退节点集合
     * @param tempList     临时存储节点集合（存储一次迭代过程中的同级userTask节点）
     * @return 回退节点集合
     */
    private List<ActivityImpl> iteratorBackActivity(String taskId,ActivityImpl currActivity, List<ActivityImpl> rtnList,
                                                    List<ActivityImpl> tempList) throws Exception {
        Task task =  taskService.createTaskQuery().taskId(taskId).singleResult();
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();

        List<PvmTransition> incomingTransitions = currActivity.getIncomingTransitions();
        List<ActivityImpl> exclusiveGateways = new ArrayList<ActivityImpl>();
        List<ActivityImpl> parallelGateways = new ArrayList<ActivityImpl>();
        for (PvmTransition pvmTransition : incomingTransitions) {
            TransitionImpl transitionImpl = (TransitionImpl) pvmTransition;
            ActivityImpl activityImpl = transitionImpl.getSource();
            String type = (String) activityImpl.getProperty("type");
            /**
             * 并行节点配置要求：<br>
             * 必须成对出现，且要求分别配置节点ID为:XXX_start(开始)，XXX_end(结束)
             */
            if ("parallelGateway".equals(type)){
                // 并行路线
                String gatewayId = activityImpl.getId();
                String gatewayType = gatewayId.substring(gatewayId.lastIndexOf("_") + 1);
                if ("START".equals(gatewayType.toUpperCase())) {
                    return rtnList;
                } else {
                    parallelGateways.add(activityImpl);
                }
            } else if ("startEvent".equals(type)) {
                return rtnList;
            } else if ("userTask".equals(type)) {
                tempList.add(activityImpl);
            } else if ("exclusiveGateway".equals(type)) {
                currActivity = transitionImpl.getSource();
                exclusiveGateways.add(currActivity);
            }
        }

        /**
         * 迭代条件分支集合，查询对应的userTask节点
         */
        for (ActivityImpl activityImpl : exclusiveGateways) {
            iteratorBackActivity(taskId, activityImpl, rtnList, tempList);
        }

        /**
         * 迭代并行集合，查询对应的userTask节点
         */
        for (ActivityImpl activityImpl : parallelGateways) {
            iteratorBackActivity(taskId, activityImpl, rtnList, tempList);
        }

        /**
         * 根据同级userTask集合，过滤最近发生的节点
         */
        currActivity = filterNewestActivity(processInstance, tempList);
        if (currActivity != null) {
            String id = findParallelGatewayId(currActivity);
            if (StringUtils.isEmpty(id)){
                rtnList.add(currActivity);
            } else {
                currActivity = findActivitiImpl(taskId, id);
            }
            tempList.clear();
            iteratorBackActivity(taskId, currActivity, rtnList, tempList);
        }
        return rtnList;
    }


    /**
     * 根据流入任务集合，查询最近一次的流入任务节点
     *
     * @param processInstance 流程实例
     * @param tempList        流入任务集合
     * @return
     */
    private ActivityImpl filterNewestActivity(ProcessInstance processInstance, List<ActivityImpl> tempList) {
        while (tempList.size() > 0) {
            ActivityImpl activity_1 = tempList.get(0);
            HistoricActivityInstance activityInstance_1 = findHistoricUserTask(processInstance, activity_1.getId());
            if (activityInstance_1 == null) {
                tempList.remove(activity_1);
                continue;
            }
            if (tempList.size() > 1) {
                ActivityImpl activity_2 = tempList.get(1);
                HistoricActivityInstance activityInstance_2 = findHistoricUserTask(processInstance, activity_2.getId());
                if (activityInstance_2 == null) {
                    tempList.remove(activity_2);
                    continue;
                }

                if (activityInstance_1.getEndTime().before(activityInstance_2.getEndTime())) {
                    tempList.remove(activity_1);
                } else {
                    tempList.remove(activity_2);
                }
            } else {
                break;
            }
        }
        if (tempList.size() > 0) {
            return tempList.get(0);
        }
        return null;
    }

    /**
     * 根据当前节点，查询输出流向是否为并行终点，如果为并行终点，则拼装对应的并行起点ID
     *
     * @param activityImpl 当前节点
     * @return
     */
    private String findParallelGatewayId(ActivityImpl activityImpl) {
        List<PvmTransition> incomingTransitions = activityImpl.getOutgoingTransitions();
        for (PvmTransition pvmTransition : incomingTransitions) {
            TransitionImpl transitionImpl = (TransitionImpl) pvmTransition;
            activityImpl = transitionImpl.getDestination();
            String type = (String) activityImpl.getProperty("type");
            if ("parallelGateway".equals(type)) {
                String gatewayId = activityImpl.getId();
                String gatewayType = gatewayId.substring(gatewayId.lastIndexOf("_") + 1);
                if ("END".equals(gatewayType.toUpperCase())) {
                    return gatewayId.substring(0, gatewayId.lastIndexOf("_")) + "_start";
                }
            }
        }
        return null;
    }

    /**
     * 查询指定任务节点的最新记录
     *
     * @param processInstance 流程实例
     * @param activityId
     * @return
     */
    private HistoricActivityInstance findHistoricUserTask(
            ProcessInstance processInstance, String activityId) {
        HistoricActivityInstance rtnVal = null;
        List<HistoricActivityInstance> historicActivityInstances = historyService.createHistoricActivityInstanceQuery().activityType("userTask")
                .processInstanceId(processInstance.getId()).activityId(activityId).finished()
                .orderByHistoricActivityInstanceEndTime().desc().list();
        if (historicActivityInstances.size() > 0) {
            rtnVal = historicActivityInstances.get(0);
        }
        return rtnVal;
    }


    public void doBackTheTask(Map<String, Object> param) {
        String processInstanceId = (String) param.get("processInstanceId");
        String activityId = (String) param.get("activityId");
        Map<String, Object> variables;
        ExecutionEntity entity = (ExecutionEntity) runtimeService.createExecutionQuery().executionId(processInstanceId).singleResult();
        ProcessDefinitionEntity definition = (ProcessDefinitionEntity)((RepositoryServiceImpl)repositoryService).getDeployedProcessDefinition(entity.getProcessDefinitionId());
        variables = entity.getProcessVariables();
        ActivityImpl currActivityImpl = definition.findActivity(entity.getActivityId());
        ActivityImpl nextActivityImpl = definition.findActivity(activityId);
        if(currActivityImpl !=null) {
            List<PvmTransition> pvmTransitions = currActivityImpl.getOutgoingTransitions();
            List<PvmTransition> oriPvmTransitions = new ArrayList<PvmTransition>();
            for (PvmTransition transition : pvmTransitions) {
                oriPvmTransitions.add(transition);
            }
            pvmTransitions.clear();
            List<TransitionImpl> transitionImpls = new ArrayList<TransitionImpl>();
            TransitionImpl tImpl = currActivityImpl.createOutgoingTransition();
            tImpl.setDestination(nextActivityImpl);
            transitionImpls.add(tImpl);
            List<Task> list = taskService.createTaskQuery().processInstanceId(entity.getProcessInstanceId())
                    .taskDefinitionKey(entity.getActivityId()).list();
            for (Task task : list) {
                String comment = MessageUtils.getMessage(ErrorCode.ROLLBACK_MESSAGE);
                if(StringUtils.isNotEmpty((String)param.get("comment"))){
                    comment += "[" + (String)param.get("comment")+"]";
                }
                taskService.addComment(task.getId(), task.getProcessInstanceId(), comment);
                taskService.complete(task.getId(), variables);
                //historyService.deleteHistoricTaskInstance(task.getId());
            }
            for (TransitionImpl transitionImpl : transitionImpls) {
                currActivityImpl.getOutgoingTransitions().remove(transitionImpl);
            }

            for (PvmTransition pvmTransition : oriPvmTransitions) {
                pvmTransitions.add(pvmTransition);
            }
        }
    }

    /**
     * 删除任务下的所有变量
     *
     * @param taskId
     * @return
     */
    public void deleteTaskVariables(String taskId) {
        Collection<String> currentVariables = taskService.getVariablesLocal(taskId).keySet();
        taskService.removeVariables(taskId, currentVariables);
    }

    public void deleteTaskVariable(Map<String, Object> param) {
        String scope = (String) param.get("scope");
        if (null != scope) {
            if ("global".equals(scope.toLowerCase())) {
                Task task = taskService.createTaskQuery().taskId((String) param.get("taskId")).singleResult();
                runtimeService.removeVariable(task.getExecutionId(), (String) param.get("variableName"));
            } else if ("local".equals(scope.toLowerCase())) {
                taskService.removeVariable((String) param.get("taskId"), (String) param.get("variableName"));
            }
        } else {
            taskService.removeVariable((String) param.get("taskId"), (String) param.get("variableName"));
        }
    }

    /**
     * 任务回退至上一用户任务节点
     *
     * @param param
     * @return
     */
    public void doBackPreviousTask(Map<String, Object> param) {
        String taskId = (String) param.get("taskId");
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        HistoryService historyService = processEngine.getHistoryService();
        RuntimeService runtimeService = processEngine.getRuntimeService();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        Map<String, Object> variables = null;

        HistoricTaskInstance currTask = historyService.createHistoricTaskInstanceQuery()
                .taskId(taskId).singleResult();
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(currTask.getProcessInstanceId()).singleResult();
        ProcessDefinitionEntity processDefinitionEntity = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
                .getDeployedProcessDefinition(currTask.getProcessDefinitionId());
        ActivityImpl currActivity = (processDefinitionEntity).findActivity(currTask.getTaskDefinitionKey());
        List<PvmTransition> originPvmTransitionList = new ArrayList<PvmTransition>();
        List<PvmTransition> pvmTransitionList = currActivity.getOutgoingTransitions();
        for (PvmTransition pvmTransition : pvmTransitionList) {
            originPvmTransitionList.add(pvmTransition);
        }
        pvmTransitionList.clear();
        List<HistoricActivityInstance> historicActivityInstances = historyService
                .createHistoricActivityInstanceQuery().activityType("userTask")
                .processInstanceId(processInstance.getId())
                .finished().orderByHistoricActivityInstanceEndTime().desc().list();
        TransitionImpl transitionImpl = null;
        ActivityImpl lastActivity = null;
        if (historicActivityInstances.size() > 0) {
            lastActivity = processDefinitionEntity.findActivity(historicActivityInstances.get(0).getActivityId());
            transitionImpl = currActivity.createOutgoingTransition();
            transitionImpl.setDestination(lastActivity);
        }else {
            throw new RenException(ErrorCode.SUPERIOR_NOT_EXIST);
        }
        variables = processInstance.getProcessVariables();
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstance.getId())
                .taskDefinitionKey(currTask.getTaskDefinitionKey()).list();
        for (Task task : tasks) {
            String comment = MessageUtils.getMessage(ErrorCode.ROLLBACK_MESSAGE);
            if(StringUtils.isNotEmpty((String)param.get("comment"))){
                comment += "[" + (String)param.get("comment")+"]";
            }
            taskService.addComment(task.getId(), task.getProcessInstanceId(), comment);
            taskService.complete(task.getId(), variables);
            //historyService.deleteHistoricTaskInstance(task.getId());
        }
        currActivity.getOutgoingTransitions().remove(transitionImpl);
        for (PvmTransition pvmTransition : originPvmTransitionList) {
            pvmTransitionList.add(pvmTransition);
        }
    }

    public void endProcess(Map<String, Object> param) {
        String taskId = (String) param.get("taskId");
        String comment = (String) param.get("comment");
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        ActivityImpl endActivity = findActivitiImpl(taskId, "end");
        if (endActivity == null) {
            return;
        }
        Map<String, Object> variables = new HashMap<>();
        if (!StringUtils.isEmpty(comment)) {
            variables.put("终止意见", comment);
        }
        if (StringUtils.isEmpty(endActivity.getId())) {
            if (!StringUtils.isEmpty(task.getOwner())) {
                taskService.resolveTask(task.getId(), variables);
            }
            taskService.complete(taskId, variables);
        } else {
            ActivityImpl currActivity = findActivitiImpl(taskId, null);
            List<PvmTransition> oriPvmTransitionList = new ArrayList<PvmTransition>();
            List<PvmTransition> pvmTransitionList = currActivity
                    .getOutgoingTransitions();
            for (PvmTransition pvmTransition : pvmTransitionList) {
                oriPvmTransitionList.add(pvmTransition);
            }
            pvmTransitionList.clear();
            TransitionImpl newTransition = currActivity.createOutgoingTransition();
            ActivityImpl pointActivity = findActivitiImpl(taskId, endActivity.getId());
            newTransition.setDestination(pointActivity);
            if (!StringUtils.isEmpty(task.getOwner())) {
                taskService.resolveTask(task.getId(), variables);
            }
            taskService.complete(taskId, variables);
            pointActivity.getIncomingTransitions().remove(newTransition);
            List<PvmTransition> pvmTransitionListC = currActivity.getOutgoingTransitions();
            pvmTransitionListC.clear();
            for (PvmTransition pvmTransition : oriPvmTransitionList) {
                pvmTransitionListC.add(pvmTransition);
            }
        }

    }

    private ActivityImpl findActivitiImpl(String taskId, String activityId) {
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        if (task == null) {
            return null;
        }
        ProcessDefinitionEntity processDefinition = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService).getDeployedProcessDefinition(task.getProcessDefinitionId());
        if (processDefinition == null) {
            throw new RenException(ErrorCode.NONE_EXIST_PROCESS);
        }
        if (StringUtils.isEmpty(activityId)) {
            activityId = task.getTaskDefinitionKey();
        }
        if ("END".equals(activityId.toUpperCase())) {
            for (ActivityImpl activityImpl : processDefinition.getActivities()) {
                List<PvmTransition> pvmTransitionList = activityImpl.getOutgoingTransitions();
                if (pvmTransitionList.isEmpty()) {
                    return activityImpl;
                }
            }
        }
        ActivityImpl activityImpl = processDefinition.findActivity(activityId);
        return activityImpl;
    }

    /**
     * 下一个用户任务节点信息,
     * <p>
     * 如果下一个节点为用户任务则直接返回,
     * <p>
     * 如果下一个节点为排他网关, 获取排他网关Id信息, 根据排他网关Id信息和execution获取流程实例排他网关Id为key的变量值,
     * 根据变量值分别执行排他网关后线路中的el表达式, 并找到el表达式通过的线路后的用户任务
     * <p>
     * 如果下一节点是并行网关（parallelGateway），如何处理？
     * <p/>
     * 如果下一节点是包容网关（inclusiveGateway），如何处理？
     *
     * @param activityImpl      流程节点信息
     * @param activityId        当前流程节点Id信息
     * @param elString          排他网关顺序流线段判断条件
     * @param processInstanceId 流程实例Id信息
     * @return
     */
    private TaskDefinition nextTaskDefinition(ActivityImpl activityImpl, String activityId, String elString, String processInstanceId) {

        PvmActivity ac = null;
        Object s = null;
        if ("userTask".equals(activityImpl.getProperty("type")) && !activityId.equals(activityImpl.getId())) {
            TaskDefinition taskDefinition = ((UserTaskActivityBehavior) activityImpl.getActivityBehavior())
                    .getTaskDefinition();
            return taskDefinition;
        } else if ("exclusiveGateway".equals(activityImpl.getProperty("type"))) {
            List<PvmTransition> outTransitions = activityImpl.getOutgoingTransitions();
            elString = getGatewayCondition(activityImpl.getId(), processInstanceId);
            if (outTransitions.size() == 1) {
                return nextTaskDefinition((ActivityImpl) outTransitions.get(0).getDestination(), activityId,
                        elString, processInstanceId);
            } else if (outTransitions.size() > 1) {
                for (PvmTransition tr1 : outTransitions) {
                    s = tr1.getProperty("conditionText");
                    if (isCondition(activityImpl.getId(), StringUtils.trim(s.toString()), elString)) {
                        return nextTaskDefinition((ActivityImpl) tr1.getDestination(), activityId, elString,
                                processInstanceId);
                    }
                }
            }
        } else {
            List<PvmTransition> outTransitions = activityImpl.getOutgoingTransitions();
            List<PvmTransition> outTransitionsTemp = null;
            for (PvmTransition tr : outTransitions) {
                ac = tr.getDestination();
                if ("exclusiveGateway".equals(ac.getProperty("type"))) {
                    outTransitionsTemp = ac.getOutgoingTransitions();
                    if (StringUtils.isEmpty(elString)) {
                        elString = getGatewayCondition(ac.getId(), processInstanceId);
                    }
                    if (outTransitionsTemp.size() == 1) {
                        return nextTaskDefinition((ActivityImpl) outTransitionsTemp.get(0).getDestination(), activityId,
                                elString, processInstanceId);
                    } else if (outTransitionsTemp.size() > 1) {
                        for (PvmTransition tr1 : outTransitionsTemp) {
                            s = tr1.getProperty("conditionText");
                            if (isCondition(ac.getId(), StringUtils.trim(s.toString()), elString)) {
                                return nextTaskDefinition((ActivityImpl) tr1.getDestination(), activityId, elString,
                                        processInstanceId);
                            }
                        }
                    }
                } else if ("userTask".equals(ac.getProperty("type"))) {
                    return ((UserTaskActivityBehavior) ((ActivityImpl) ac).getActivityBehavior()).getTaskDefinition();
                }
            }
            return null;
        }
        return null;
    }

    /**
     * 转换Task对象
     * @param task
     * @param dto
     */
    private void convertTaskInfo(Task task, TaskDTO dto) {
        dto.setTaskId(task.getId());
        dto.setTaskName(task.getName());
        dto.setActivityId(task.getExecutionId());
        dto.setAssignee(task.getAssignee());
        dto.setProcessDefinitionId(task.getProcessDefinitionId());
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionId(task.getProcessDefinitionId()).singleResult();
        dto.setProcessDefinitionName(processDefinition.getName());
        dto.setProcessDefinitionKey(processDefinition.getKey());
        HistoricProcessInstance processInstance =  historyService.createHistoricProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();
        dto.setStartTime(processInstance.getStartTime());
        dto.setBusinessKey(processInstance.getBusinessKey());
        dto.setProcessInstanceId(task.getProcessInstanceId());
        dto.setOwner(task.getOwner());
        dto.setCreateTime(task.getCreateTime());
        dto.setDueDate(task.getDueDate());
    }

    /**
     * 驳回至第一个用户任务
     * @param param
     */
    public void backToFristStep(Map<String, Object> param) {
        String taskId = (String)param.get("taskId");
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        HistoryService historyService = processEngine.getHistoryService();
        RuntimeService runtimeService = processEngine.getRuntimeService();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        Map variables = null;
        HistoricTaskInstance currTask = historyService.createHistoricTaskInstanceQuery()
                .taskId(taskId)
                .singleResult();
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(currTask.getProcessInstanceId())
                .singleResult();
        ProcessDefinitionEntity processDefinitionEntity = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
                .getDeployedProcessDefinition(currTask.getProcessDefinitionId());
        if (processDefinitionEntity == null) {
            throw new RenException(ErrorCode.NONE_EXIST_PROCESS);
        }

        ActivityImpl currActivity = processDefinitionEntity.findActivity(currTask.getTaskDefinitionKey());

        List<PvmTransition> originPvmTransitionList = new ArrayList<>();
        List<PvmTransition> pvmTransitionList = currActivity.getOutgoingTransitions();
        for (PvmTransition pvmTransition : pvmTransitionList) {
            originPvmTransitionList.add(pvmTransition);
        }
        pvmTransitionList.clear();

        List<HistoricActivityInstance> historicActivityInstances = historyService
                .createHistoricActivityInstanceQuery().activityType("userTask")
                .processInstanceId(processInstance.getId())
                .finished()
                .orderByHistoricActivityInstanceEndTime().asc().list();
        TransitionImpl transitionImpl = null;
        if (historicActivityInstances.size() > 0) {
            ActivityImpl lastActivity = processDefinitionEntity.findActivity(historicActivityInstances.get(0).getActivityId());

            transitionImpl = currActivity.createOutgoingTransition();
            transitionImpl.setDestination(lastActivity);
        }else
        {
            throw new RenException(ErrorCode.SUPERIOR_NOT_EXIST);
        }
        variables = processInstance.getProcessVariables();
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstance.getId())
                .taskDefinitionKey(currTask.getTaskDefinitionKey()).list();
        for (Task task : tasks) {
            String comment = MessageUtils.getMessage(ErrorCode.REJECT_MESSAGE);
            if(StringUtils.isNotEmpty((String)param.get("comment"))){
                comment += "[" + (String)param.get("comment")+"]";
            }
            taskService.addComment(currTask.getId(), currTask.getProcessInstanceId(), comment);
            taskService.complete(task.getId(), variables);
        }
        currActivity.getOutgoingTransitions().remove(transitionImpl);

        for (PvmTransition pvmTransition : originPvmTransitionList) {
            pvmTransitionList.add(pvmTransition);
        }
    }

    public void completeTaskByVariables(TaskDTO taskDTO) {
        if(null != taskDTO.getParams()){
            Set keySet = taskDTO.getParams().keySet();
            Iterator iterator = keySet.iterator();
            while(iterator.hasNext()){
                String key = (String)iterator.next();
                this.setTaskVariable(taskDTO.getTaskId(),key , taskDTO.getParams().get(key));
            }
        }
        this.completeTask(taskDTO.getTaskId(), taskDTO.getComment());
    }
}
